#!/bin/bash

cd test/terraform
terraform destroy --auto-approve
cd ..
cd ..
cd prod/terraform
terraform destroy --auto-approve
