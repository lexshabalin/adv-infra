terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "vault" {
  address = "http://192.168.1.17:8200"
#  address = "http://109.111.174.147:8200"
  skip_tls_verify = true
}

#########################################################################################################################################################################
######################################################################## HASHICORP VAULT SECRETS ########################################################################
#########################################################################################################################################################################

data "vault_generic_secret" "yc_secrets" {
  path = "adv/yc_secrets"
}

provider "yandex" {
  token     = data.vault_generic_secret.yc_secrets.data["YC_TOKEN"]
  cloud_id  = data.vault_generic_secret.yc_secrets.data["YC_CLOUD_ID"]
  folder_id = data.vault_generic_secret.yc_secrets.data["YC_FOLDER_ID"]
  zone      = "ru-central1-a"
}

data "yandex_compute_image" "img" {
  image_id = data.vault_generic_secret.yc_secrets.data["IMAGE_ID"]
}

data "yandex_compute_image" "img-elastic" {
  image_id = data.vault_generic_secret.yc_secrets.data["IMAGE_ID_ELASTIC"]
}

data "yandex_compute_image" "img-kibana" {
  image_id = data.vault_generic_secret.yc_secrets.data["IMAGE_ID_KIBANA"]
}

#########################################################################################################################################################################
######################################################################### CONSUL CLUSTER CONFIG #########################################################################
#########################################################################################################################################################################

############################################ SPECS consul-srv-VM1
resource "yandex_compute_instance" "consul-vm1" {
  name        = "test-consul-vm1"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulsrv.hcl-template"
    destination = "/home/rh/consulsrv.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname test-consul-vm1",
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulsrvcfg.yml -vv",
      "ansible-playbook -b -c local -i localhost, consulsrvaddui.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
}


############################################ SPECS consul-srv-VM2
resource "yandex_compute_instance" "consul-vm2" {
  name        = "test-consul-vm2"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulsrv.hcl-template"
    destination = "/home/rh/consulsrv.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname test-consul-vm2",
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulsrvcfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
}

############################################ SPECS consul-srv-VM3
resource "yandex_compute_instance" "consul-vm3" {
  name        = "test-consul-vm3"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulsrv.hcl-template"
    destination = "/home/rh/consulsrv.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }
  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname test-consul-vm3",
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulsrvcfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
}

#########################################################################################################################################################################
####################################################################### ELASTICSEARCH MASTER NODE #######################################################################
#########################################################################################################################################################################

resource "yandex_compute_instance" "elastic-vm1" {
  name        = "test-elastic-vm1"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img-elastic.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulcli.hcl-template"
    destination = "/home/rh/consulcli.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/elasticsearch-curator_5.8.4_amd64.deb"
    destination = "/home/rh/elasticsearch-curator_5.8.4_amd64.deb"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname test-elastic1",
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulclicfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply",
      "cd /home/rh/adv-ansiblescripts2/monitoring/elasticsearch",
      "ansible-playbook -b -c local -i localhost, elasticsearch_master_cfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/monitoring/filebeat",
      "ansible-playbook -b -c local -i localhost, filebeatcfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/monitoring/curator",
      "ansible-playbook -b -c local -i localhost, curatorcfg.yml -vvvv"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
  depends_on = [yandex_compute_instance.consul-vm3]
}

#########################################################################################################################################################################
################################################################################# KIBANA ################################################################################
#########################################################################################################################################################################

resource "yandex_compute_instance" "kibana-vm" {
  name        = "test-kibana-vm"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 4
    memory = 8
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img-kibana.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulsrv.hcl-template"
    destination = "/home/rh/consulsrv.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulcli.hcl-template"
    destination = "/home/rh/consulcli.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname test-kibana-vm",
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulclicfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply",
      "cd /home/rh/adv-ansiblescripts2/monitoring/kibana",
      "ansible-playbook -b -c local -i localhost, kibana-cfg.yml -vv",
      "/bin/sleep 30",
      "cd /home/rh/adv-ansiblescripts2/monitoring/filebeat",
      "ansible-playbook -b -c local -i localhost, filebeatcfg.yml -vv"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
  depends_on = [yandex_compute_instance.elastic-vm1]
}

#########################################################################################################################################################################
######################################################################### ELASTICSEARCH CLUSTER #########################################################################
#########################################################################################################################################################################

resource "yandex_compute_instance" "elastic-vm2" {
  name        = "test-elastic-vm2"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img-elastic.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulcli.hcl-template"
    destination = "/home/rh/consulcli.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname test-elastic2",
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulclicfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply",
      "cd /home/rh/adv-ansiblescripts2/monitoring/elasticsearch",
      "ansible-playbook -b -c local -i localhost, elasticsearch_node1_cfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/monitoring/filebeat",
      "ansible-playbook -b -c local -i localhost, filebeatcfg.yml -vv"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
  depends_on = [yandex_compute_instance.elastic-vm1]
}


resource "yandex_compute_instance" "elastic-vm3" {
  name        = "test-elastic-vm3"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img-elastic.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulcli.hcl-template"
    destination = "/home/rh/consulcli.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname test-elastic3",
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulclicfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply",
      "cd /home/rh/adv-ansiblescripts2/monitoring/elasticsearch",
      "ansible-playbook -b -c local -i localhost, elasticsearch_node2_cfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/monitoring/filebeat",
      "ansible-playbook -b -c local -i localhost, filebeatcfg.yml -vv"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
  depends_on = [yandex_compute_instance.elastic-vm1]
}

#########################################################################################################################################################################                                                  
########################################################################## ASG CONFIG ###################################################################################
#########################################################################################################################################################################

resource "yandex_compute_instance_group" "service" {
  name                = "service"
  folder_id           = data.vault_generic_secret.yc_secrets.data["YC_FOLDER_ID"]
  service_account_id  = data.vault_generic_secret.yc_secrets.data["YC_SRV_ACC_ID"]
  deletion_protection = false
  instance_template {
    platform_id = "standard-v1"
    resources {
      memory = 2
      cores  = 2
    }
    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = data.vault_generic_secret.yc_secrets.data["IMAGE_ID"]
        size     = 15
      }
    }
    network_interface {
      network_id = yandex_vpc_network.my-nw-1.id
      subnet_ids = ["${yandex_vpc_subnet.my-sn-1.id}"]
      nat        = true
    }
    labels = {
      label1 = "label1-value"
      label2 = "label2-value"
    }
    metadata = {
      user-data = "${file("./metadata-asg-service.txt")}"
    }
  }

  scale_policy {
    auto_scale {
      measurement_duration   = 60
      initial_size           = 1
      cpu_utilization_target = 75
      max_size               = 3
      warmup_duration        = 60
      stabilization_duration = 120
    }
  }

  allocation_policy {
    zones = ["ru-central1-a"]
  }

  deploy_policy {
    max_unavailable = 2
    max_creating    = 2
    max_expansion   = 2
    max_deleting    = 2
  }

  application_load_balancer {
    target_group_name = "service"
  }

}

#########################################################################################################################################################################                                                  
######################################################################### GITLAB RUNNER CONFIG ##########################################################################
#########################################################################################################################################################################

#SPECS VM1###OLD
resource "yandex_compute_instance" "my-vm-1" {
  name        = "test-runner-vm1"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 4
    memory = 8
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulcli.hcl-template"
    destination = "/home/rh/consulcli.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  #install ansible+docker
  provisioner "remote-exec" {
    inline = [
      "sudo rm /etc/bash.bashrc",
      "sudo cp /home/rh/.bashrc /etc/bash.bashrc",
      "sudo hostnamectl set-hostname test-runner-vm1",
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulclicfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "cd /home/rh/adv-ansiblescripts2/runner",
      "ansible-playbook -b runnercfg-test.yml -vv",
      "sudo netplan apply",
      "/bin/sleep 30",
      "cd /home/rh/adv-ansiblescripts2/monitoring/filebeat",
      "ansible-playbook -b -c local -i localhost, filebeatcfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/security/sonarqube",
      "ansible-playbook -b -c local -i localhost, sonarqubecfg.yml -vv"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }
  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
  depends_on = [yandex_compute_instance.elastic-vm1]
}

#########################################################################################################################################################################                                                  
######################################################################### MONITORING VM CONFIG ##########################################################################
#########################################################################################################################################################################

##SPECS VM2_monitoring
resource "yandex_compute_instance" "my-vm-2" {
  name        = "test-monitoring-vm2"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulcli.hcl-template"
    destination = "/home/rh/consulcli.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname test-monitoring-vm2",
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulclicfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/monitoring",
      "ansible-playbook -b -c local -i localhost, monitoringcfg-test.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply",
      "/bin/sleep 30",
      "cd /home/rh/adv-ansiblescripts2/monitoring/filebeat",
      "ansible-playbook -b -c local -i localhost, filebeatcfg.yml -vv"

    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
  depends_on = [yandex_compute_instance.elastic-vm1]
}

#########################################################################################################################################################################                                                  
############################################################################ OTHER STUFF ################################################################################
#########################################################################################################################################################################

############################################ NETWORK
resource "yandex_vpc_network" "my-nw-1" {
  name = "my-nw-1"
}

resource "yandex_dns_zone" "zone1" {
  name        = "my-public-zone"
  description = "desc"

  labels = {
    label1 = "label-1-value"
  }

  zone   = "test.oorange.space."
  public = true
}

############################################ APPLICATION-LOAD-BALANCER

##### backend group #####
resource "yandex_alb_backend_group" "service-bg" {
  name = "service-bg"
  http_backend {
    name             = "backend-1"
    port             = 8080
    target_group_ids = [yandex_compute_instance_group.service.application_load_balancer.0.target_group_id]
    healthcheck {
      timeout          = "10s"
      interval         = "2s"
      healthcheck_port = 8080
      http_healthcheck {
        path = "/"
      }
    }
  }
}

##### http router Domain #####
resource "yandex_alb_http_router" "service-router" {
  name = "service-router"
}

resource "yandex_alb_virtual_host" "service-host" {
  name           = "service-host"
  http_router_id = yandex_alb_http_router.service-router.id
  authority      = ["oorange.space"]
  route {
    name = "route-1"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.service-bg.id
      }
    }
  }
}

##### ALB #####
resource "yandex_alb_load_balancer" "alb-1" {
  name = "my-load-balancer"

  network_id = yandex_vpc_network.my-nw-1.id

  allocation_policy {
    location {
      zone_id   = "ru-central1-a"
      subnet_id = yandex_vpc_subnet.my-sn-1.id
    }
  }

  listener {
    name = "alb-listener-1"
    endpoint {
      address {
        external_ipv4_address {
        }
      }
      ports = [8080]
    }
    http {
      handler {
        http_router_id = yandex_alb_http_router.service-router.id
      }
    }
  }

  log_options {
    discard_rule {
      http_code_intervals = ["HTTP_ALL"]
      discard_percent     = 75
    }
  }

}

############################################ DNS

#DNS-overall
resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "srv.test.oorange.space."
  type    = "A"
  ttl     = 200
  data    = ["10.1.0.1"]
}

#DNS for elastic1
resource "yandex_dns_recordset" "elastic1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.elastic1"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.elastic-vm1.network_interface.0.nat_ip_address]
}

#DNS for elastic2
resource "yandex_dns_recordset" "elastic2" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.elastic2"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.elastic-vm2.network_interface.0.nat_ip_address]
}

#DNS for elastic3
resource "yandex_dns_recordset" "elastic3" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.elastic3"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.elastic-vm3.network_interface.0.nat_ip_address]
}

#DNS for consul
resource "yandex_dns_recordset" "consul" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.consul"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.consul-vm1.network_interface.0.nat_ip_address]
}

#DNS for kibana
resource "yandex_dns_recordset" "kibana" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.kibana"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.kibana-vm.network_interface.0.nat_ip_address]
}

#DNS for service
resource "yandex_dns_recordset" "service" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance_group.service.instances[0].network_interface[0].nat_ip_address]
}

#DNS for monitoring
resource "yandex_dns_recordset" "monitoring" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.monitoring"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.my-vm-2.network_interface.0.nat_ip_address]
}

#DNS for sonarqube-vm
resource "yandex_dns_recordset" "sonarqube" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.sonarqube"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.my-vm-1.network_interface.0.nat_ip_address]
}

############################################ SUBNET
resource "yandex_vpc_subnet" "my-sn-1" {
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.my-nw-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

#########################################################################################################################################################################                                                  
################################################################################# OUTPUT ################################################################################
#########################################################################################################################################################################

###EXTERNAL

output "external_ip_address_kibana-vm" {
  value = yandex_compute_instance.kibana-vm.network_interface.0.nat_ip_address
}

output "external_ip_address_elastic-vm1" {
  value = yandex_compute_instance.elastic-vm1.network_interface.0.nat_ip_address
}

output "external_ip_address_elastic-vm2" {
  value = yandex_compute_instance.elastic-vm2.network_interface.0.nat_ip_address
}

output "external_ip_address_elastic-vm3" {
  value = yandex_compute_instance.elastic-vm3.network_interface.0.nat_ip_address
}

output "external_ip_address_vm_test-1-runner" {
  value = yandex_compute_instance.my-vm-1.network_interface.0.nat_ip_address
}

output "external_ip_address_vm_test-2-monitoring" {
  value = yandex_compute_instance.my-vm-2.network_interface.0.nat_ip_address
}

output "external_ip_address_test_consul-vm1" {
  value = yandex_compute_instance.consul-vm1.network_interface.0.nat_ip_address
}

output "external_ip_address_test_consul-vm2" {
  value = yandex_compute_instance.consul-vm2.network_interface.0.nat_ip_address
}

output "external_ip_address_test_consul-vm3" {
  value = yandex_compute_instance.consul-vm3.network_interface.0.nat_ip_address
}

###INTERNAL

output "internal_ip_address_kibana-vm" {
  value = yandex_compute_instance.kibana-vm.network_interface.0.ip_address
}

output "internal_ip_address_elastic-vm1" {
  value = yandex_compute_instance.elastic-vm1.network_interface.0.ip_address
}

output "internal_ip_address_elastic-vm2" {
  value = yandex_compute_instance.elastic-vm2.network_interface.0.ip_address
}

output "internal_ip_address_elastic-vm3" {
  value = yandex_compute_instance.elastic-vm3.network_interface.0.ip_address
}

output "internal_ip_address_vm_test-1-runner" {
  value = yandex_compute_instance.my-vm-1.network_interface.0.ip_address
}

output "internal_ip_address_vm_test-2-monitoring" {
  value = yandex_compute_instance.my-vm-2.network_interface.0.ip_address
}

output "internal_ip_address_test_consul-vm1" {
  value = yandex_compute_instance.consul-vm1.network_interface.0.ip_address
}

output "internal_ip_address_test_consul-vm2" {
  value = yandex_compute_instance.consul-vm2.network_interface.0.ip_address
}

output "internal_ip_address_test_consul-vm3" {
  value = yandex_compute_instance.consul-vm3.network_interface.0.ip_address
}

#output "internal_ip_address_sonarqube-vm" {
#  value = yandex_compute_instance.sonarqube-vm.network_interface.0.ip_address
#}
