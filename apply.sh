#!/bin/bash

cd test/terraform
terraform fmt
terraform init
terraform apply --auto-approve
cd ..
cd ..
cd prod/terraform
terraform fmt
terraform init
terraform apply --auto-approve

