# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/vault" {
  version = "3.15.2"
  hashes = [
    "h1:UtiROE2FqvHzLNT8A8/IiD+umjeDAJFxDXybUup8HJY=",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version = "0.90.0"
  hashes = [
    "h1:exvsVHv0an0mnNP0TYy0GQdup15EhNOe+5q0yOWk1iM=",
  ]
}
