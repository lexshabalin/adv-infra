terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "vault" {
  #  address = "http://192.168.0.18:8200"
  address         = "http://109.111.174.147:8200"
  skip_tls_verify = true
}

############################################ KV v1 WORKS!
data "vault_generic_secret" "yc_secrets" {
  path = "adv/yc_secrets"
}

provider "yandex" {
  token     = data.vault_generic_secret.yc_secrets.data["YC_TOKEN"]
  cloud_id  = data.vault_generic_secret.yc_secrets.data["YC_CLOUD_ID"]
  folder_id = data.vault_generic_secret.yc_secrets.data["YC_FOLDER_ID"]
  zone      = "ru-central1-a"
}

data "yandex_compute_image" "img" {
  image_id = data.vault_generic_secret.yc_secrets.data["IMAGE_ID"]
}

#########################################################################################################################################################################
######################################################################### CONSUL CLUSTER CONFIG #########################################################################
#########################################################################################################################################################################

############################################ SPECS consul-srv-VM1
resource "yandex_compute_instance" "prod-consul-vm1" {
  name        = "prod-consul-vm1"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-2.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulsrv.hcl-template"
    destination = "/home/rh/consulsrv.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulsrvcfg.yml -vv",
      "ansible-playbook -b -c local -i localhost, consulsrvaddui.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
}


############################################ SPECS consul-srv-VM2
resource "yandex_compute_instance" "prod-consul-vm2" {
  name        = "prod-consul-vm2"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-2.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulsrv.hcl-template"
    destination = "/home/rh/consulsrv.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulsrvcfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
}

############################################ SPECS consul-srv-VM3
resource "yandex_compute_instance" "prod-consul-vm3" {
  name        = "prod-consul-vm3"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-2.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulsrv.hcl-template"
    destination = "/home/rh/consulsrv.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }
  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulsrvcfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
}

#########################################################################################################################################################################                                                  
######################################################################### GITLAB RUNNER CONFIG ##########################################################################
#########################################################################################################################################################################

#SPECS VM1
resource "yandex_compute_instance" "prod-runner" {
  name        = "prod-runner"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-2.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/consulcli.hcl-template"
    destination = "/home/rh/consulcli.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  #install ansible+docker
  provisioner "remote-exec" {
    inline = [
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulclicfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "cd /home/rh/adv-ansiblescripts2/runner",
      "ansible-playbook -b runnercfg-prod.yml -vv",
      "sudo netplan apply"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }
  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
}

#########################################################################################################################################################################                                                  
######################################################################### MONITORING VM CONFIG ##########################################################################
#########################################################################################################################################################################

##SPECS VM2_monitoring
resource "yandex_compute_instance" "prod-monitoring" {
  name        = "prod-monitoring"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.img.id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-2.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulcli.hcl-template"
    destination = "/home/rh/consulcli.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "cd /home/rh/",
      "git clone https://gitlab.com/lexshabalin/adv-ansiblescripts2.git",
      "cd /home/rh/adv-ansiblescripts2/monitoring",
      "ansible-playbook -b -c local -i localhost, monitoringcfg-prod.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/consul",
      "ansible-playbook -b -c local -i localhost, consulclicfg.yml -vv",
      "cd /home/rh/adv-ansiblescripts2/everybody",
      "ansible-playbook -b -c local -i localhost, playbook.yml -vv",
      "sudo cp /home/rh/adv-ansiblescripts2/consul/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }
}

#########################################################################################################################################################################                                                  
########################################################################## ASG CONFIG ###################################################################################
#########################################################################################################################################################################

resource "yandex_compute_instance_group" "service" {
  name                = "service"
  folder_id           = data.vault_generic_secret.yc_secrets.data["YC_FOLDER_ID"]
  service_account_id  = data.vault_generic_secret.yc_secrets.data["YC_SRV_ACC_ID"]
  deletion_protection = false
  instance_template {
    platform_id = "standard-v1"
    resources {
      memory = 2
      cores  = 2
    }
    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = data.vault_generic_secret.yc_secrets.data["IMAGE_ID"]
        size     = 15
      }
    }
    network_interface {
      network_id = yandex_vpc_network.my-nw-2.id
      subnet_ids = ["${yandex_vpc_subnet.my-sn-2.id}"]
      nat        = true
    }
    labels = {
      label1 = "label1-value"
      label2 = "label2-value"
    }
    metadata = {
      user-data = "${file("./metadata-asg-service.txt")}"
    }
  }

  scale_policy {
    auto_scale {
      measurement_duration   = 60
      initial_size           = 1
      cpu_utilization_target = 75
      max_size               = 3
      warmup_duration        = 60
      stabilization_duration = 120
    }
  }

  allocation_policy {
    zones = ["ru-central1-a"]
  }

  deploy_policy {
    max_unavailable = 2
    max_creating    = 2
    max_expansion   = 2
    max_deleting    = 2
  }

  load_balancer {
    target_group_name = "service"
  }

}

#########################################################################################################################################################################                                                  
############################################################################ OTHER STUFF ################################################################################
#########################################################################################################################################################################

############################################ NETWORK
resource "yandex_vpc_network" "my-nw-2" {
  name = "my-nw-2"
}

resource "yandex_dns_zone" "zone2" {
  name        = "my-public-zone"
  description = "desc"

  labels = {
    label1 = "label-1-value"
  }

  zone   = "oorange.space."
  public = true
}

############################################ LOAD-BALANCER
resource "yandex_lb_network_load_balancer" "lb-2" {
  name = "network-load-balancer-2"

  listener {
    name = "network-load-balancer-3-listener"
    port = 8080
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  listener {
    name = "network-load-balancer-4-listener"
    port = 9100
    external_address_spec {
      ip_version = "ipv4"
    }
  }


  attached_target_group {
    target_group_id = yandex_compute_instance_group.service.load_balancer.0.target_group_id

    healthcheck {
      name = "http"
      http_options {
        port = 8080
        path = "/index.html"
      }
    }
  }
}

############################################ DNS

#DNS-overall
resource "yandex_dns_recordset" "rs2" {
  zone_id = yandex_dns_zone.zone2.id
  name    = "srv.oorange.space."
  type    = "A"
  ttl     = 200
  data    = ["10.1.0.1"]
}

#DNS for consul
resource "yandex_dns_recordset" "consul" {
  zone_id = yandex_dns_zone.zone2.id
  name    = "www.consul"
  type    = "A"
  ttl     = 300
  data    = [yandex_compute_instance.prod-consul-vm1.network_interface.0.nat_ip_address]
}

#DNS for service
resource "yandex_dns_recordset" "service" {
  zone_id = yandex_dns_zone.zone2.id
  name    = "www"
  type    = "A"
  ttl     = 300
  data    = [tolist(tolist(yandex_lb_network_load_balancer.lb-2.listener)[0].external_address_spec)[0].address]
}

#DNS for monitoring
resource "yandex_dns_recordset" "monitoring" {
  zone_id = yandex_dns_zone.zone2.id
  name    = "www.monitoring"
  type    = "A"
  ttl     = 300
  data    = [yandex_compute_instance.prod-monitoring.network_interface.0.nat_ip_address]
}

############################################ SUBNET
resource "yandex_vpc_subnet" "my-sn-2" {
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.my-nw-2.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

#########################################################################################################################################################################                                                  
################################################################################# OUTPUT ################################################################################
#########################################################################################################################################################################

############################################ OUTPUT

output "internal_ip_address_vm_prod-1-runner" {
  value = yandex_compute_instance.prod-runner.network_interface.0.ip_address
}

output "internal_ip_address_vm_prod-2-monitoring" {
  value = yandex_compute_instance.prod-monitoring.network_interface.0.ip_address
}

output "external_ip_address_vm_prod-1-runner" {
  value = yandex_compute_instance.prod-runner.network_interface.0.nat_ip_address
}

output "external_ip_address_vm_prod-2-monitoring" {
  value = yandex_compute_instance.prod-monitoring.network_interface.0.nat_ip_address
}

output "internal_ip_address_prod_consul-vm1" {
  value = yandex_compute_instance.prod-consul-vm1.network_interface.0.ip_address
}

output "internal_ip_address_prod_consul-vm2" {
  value = yandex_compute_instance.prod-consul-vm2.network_interface.0.ip_address
}

output "internal_ip_address_prod_consul-vm3" {
  value = yandex_compute_instance.prod-consul-vm3.network_interface.0.ip_address
}


output "external_ip_address_prod_consul-vm1" {
  value = yandex_compute_instance.prod-consul-vm1.network_interface.0.nat_ip_address
}

output "external_ip_address_prod_consul-vm2" {
  value = yandex_compute_instance.prod-consul-vm2.network_interface.0.nat_ip_address
}

output "external_ip_address_prod_consul-vm3" {
  value = yandex_compute_instance.prod-consul-vm3.network_interface.0.nat_ip_address
}

